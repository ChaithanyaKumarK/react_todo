import React from "react";
import TodoItems from "./TodoItems";
import PropTypes from "prop-types";

class Todos extends React.Component {
  render() {
    return this.props.todos.map((todos) => {
      return (
        <TodoItems
          key={todos.id}
          todo={todos}
          delTodo={this.props.delTodo}
          markComplete={this.props.markComplete}
        />
      );
    });
  }
}

//proptypes
Todos.propTypes = {
  todos: PropTypes.array.isRequired,
  markComplete: PropTypes.func.isRequired,
  delTodo: PropTypes.func.isRequired,
};
export default Todos;

//.bind(this, id)
